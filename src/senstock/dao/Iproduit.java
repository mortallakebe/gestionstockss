package senstock.dao;

import senstock.entities.Produit;


import java.util.List;

public interface Iproduit
{
    public  int add(Produit produit);
    public List<Produit> list();
}
