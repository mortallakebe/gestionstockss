package senstock.dao;

import senstock.entities.User;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class IuserImplement implements Iuser
{
    private EntityManager em;

    public IuserImplement()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("senstocks");
        em = emf.createEntityManager();
    }

    @Override
    public int registrer(User user)
    {

        try
        {
           em.getTransaction().begin();
            em.persist(user);
           em.getTransaction().commit();
           return 1;
        }catch (Exception ex)
        {
            ex.printStackTrace();

        }
        return 0;

    }

    @Override
    public List<User> login(String email, String password)
    {
        return  em.createQuery("SELECT user From User user " + " Where user.email =:email and user.password=:password").setParameter("email",email).setParameter("password", password).getResultList();

    }
}
