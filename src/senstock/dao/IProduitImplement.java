package senstock.dao;

import senstock.entities.Produit;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class IProduitImplement implements  Iproduit

{
    private EntityManager em;

    public IProduitImplement() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("senstocks");
        em = emf.createEntityManager();
    }

    @Override
    public int add(Produit produit)
    {
        try {

            em.getTransaction().begin();
            em.persist(produit);
            em.getTransaction().commit();
            return 1;
        }catch (Exception ex)
        {
            ex.printStackTrace();
            return 0;

        }

    }

    @Override
    public List<Produit> list()
    {
        return em.createQuery("SELECT p From Produit p ").getResultList();
    }
}
