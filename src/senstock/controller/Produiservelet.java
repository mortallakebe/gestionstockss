package senstock.controller;

import senstock.dao.IProduitImplement;
import senstock.dao.Iproduit;
import senstock.entities.Produit;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet( urlPatterns =  "/Produit" , name="produit")
public class Produiservelet extends HttpServlet
{

    private Iproduit produitdao;
    //initialisatio du variable Iproduit

    @Override
    public void init(ServletConfig config) throws ServletException {
        produitdao = new IProduitImplement();
    }

    protected  void  doGet(HttpServletRequest request , HttpServletResponse response) throws  ServletException ,IOException
    {
        List<Produit> produitList = produitdao.list();
        request.setAttribute("produits",produitList);
        request.getRequestDispatcher("produit/add.jsp").forward(request,response);

    }

    protected  void  doPost(HttpServletRequest request , HttpServletResponse response)throws  ServletException,IOException
    {
         //recuperation des elements du formulaire
         String nom = request.getParameter("nom").toString();
         Double qtStock = Double.parseDouble(request.getParameter("qtStock").toString());

        Produit produit = new Produit();
        produit.setNom(nom);
        produit.setQtStock(qtStock);

        int ok = produitdao.add(produit);
        request.setAttribute(   "result",ok);
        doGet(request,response);
    }

}
