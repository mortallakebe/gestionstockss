package senstock.controller;


import senstock.dao.Iuser;
import senstock.dao.IuserImplement;
import senstock.entities.User;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet( urlPatterns =  "/Registrer" , name="registrer")
public class registrerServelet extends  HttpServlet
{

    private Iuser iuser;
    //initialisatio du variable Iproduit

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        iuser = new IuserImplement();
    }

    protected  void  doGet(HttpServletRequest request,HttpServletResponse response) throws IOException , ServletException
    {
        request.getRequestDispatcher("index.jsp").forward(request,response);
    }

    protected  void  doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
         String nom;
         String email;
         String password;

         nom = request.getParameter("nom");
         email = request.getParameter("email");
         password = request.getParameter("password");

         User user = new User();
         user.setNom(nom);
         user.setEmail(email);
         user.setPassword(password);

         iuser.registrer(user);

         doGet(request,response);

    }
}
