<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp"></jsp:include>

<div class="col-lg-12 col-sm-12 col-xs-12">
<p></p>
<div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="widget">


                        <c:if test="${result == 1}">
                            <div id="message" class="alert alert-info"> Produit Ajouter avec Success</div>
                        </c:if>


                    <div class="widget-body">
                        <div class="collapse in">
                            <form method="post" action="Produit">

                                <div class="form-group">
                                    <label >nom du produit </label>
                                    <input type="text"  name="nom" class="form-control"  placeholder="Nom produit">
                                </div>

                                <div class="form-group">
                                    <label >Quantite en Stock</label>
                                    <input type="text"  name="qtStock" class="form-control"  placeholder="quantite Prouit">
                                </div>

                                <div class="registerbox-submit">
                                    <input type="submit" class="btn btn-primary pull-right" value="SUBMIT">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

    <div class="col-xs-12 col-md-6">
        <div class="well with-header  with-footer">
            <div class="header bg-blue">
                Simple Table With Hover
            </div>
            <table class="table table-hover">
                <thead class="bordered-darkorange">

                    <tr>
                        <th>ID Produit</th>
                        <th>Nom Produit</th>
                        <th>Quantite Produit</th>
                    </tr>
                    <c:forEach items="${produits}" var="produit">
                        <tr>
                            <td>${produit.id}</td>
                            <td>${produit.nom}</td>
                            <td>${produit.qtStock}</td>
                        </tr>
                    </c:forEach>


                </tbody>
            </table>

            <div class="footer">
            </div>
        </div>

    </div>
</div>

</div>



<jsp:include page="../footer.jsp"></jsp:include>